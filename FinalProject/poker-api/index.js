const express = require('express');
const bodyParser = require('body-parser')
const CardHelper = require("./src/helpers/CardHelper.js");

// App Setup
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Helper Setup
const cardHelper = new CardHelper();

// API Services
app.post('/api/cards/getCombination', (req, res) => {
    try {
        let resultingHand = cardHelper.evaluateHandCards(req);
        let resultBody = {
            Result: resultingHand
        }
        res.send(resultBody);
    }
    catch(error) {
        res.status(400).send(error);
    }
})

// PORT
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));
