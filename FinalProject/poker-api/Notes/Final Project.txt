Beertap API

Requirements
- The API should manage beer taps and their keg state
- Every office has a beer tap, can be one or many.
- Every tap can dispense beer. Each dispense changes it's state.
	- Full
	- Going down
	- Almost dry
	- Needs refill
- When the state is [Full, Going down, Almost dry], beer can be dispensed
- When the state is [Need refill], beer can no longer be dispensed
- The API must support the following:
	- /office/addKeg
	- /office/1/dispense	- means beer tap with ID = 1 dispenses beer
	- /office/1/replaceKeg	- means beer tap with ID = 1 is replaced with a new keg
- The response body should be similar to as follows:
	{
		"id" : 1,
		"state": "Going Down"
	}
	- This example response is returned after dispensing from a Full beer tap with id = 1.
	
	
	
Poker Hand API

Requirements
- The API should return the hand type out of the cards
- Supported subset of hand types are [Flush, Three of a Kind, One Pair, High Card]
- The API should accept five cards, each representing the card as follows:
	- H, C, S, D 	(hearts, clubs, spades, diamonds)
	- 1 - 10, J, Q, K, A
- The request body should be similar to as follows:
	{
		"hand" : [
			"H1", "H2", "S3", "D3", "C3"
		]
	}
	- This example represents a hand with cards 1 of hearts, 2 of hearts, 3 of Spades, 3 of Diamonds, 3 of Clubs
- The response body should be similar to as follows
	{
		"Result" : "Three of a Kind"
	}
	or
	{
		"Result" : "High Card"
	}