module.exports = class ErrorResponse {
  constructor(invalidCard, errorMessage) {
    this.invalidCards = invalidCard;
    this.error = errorMessage;
  }
}
