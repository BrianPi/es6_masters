module.exports = class Card {
  constructor(cardCode) {
    let numTextArray = [];
    this.Code = cardCode;
    [this.Suit, ...numTextArray] = cardCode;
    this.Num = getCardNumByNumText(numTextArray.join(""));
    this.NumDisplay = getCardNumTextByNum(this.Num);
    this.CardDisplay = `${this.Suit}${this.NumDisplay}`;
    this.AlternateNum = this.Num == 1 ? 14 : null;
  }
}

// PRIVATE/UNEXPOSED FUNCTIONS
const getCardNumTextByNum = cardNum => {
  switch (cardNum.toString()) {
    case "1":
      return "A";
    case "11":
      return "J";
    case "12":
      return "Q";
    case "13":
      return "K";
    default:
      return cardNum.toString();
  }
};

const getCardNumByNumText = cardNum => {
  switch (cardNum.toString()) {
    case "A":
      return 1;
    case "J":
      return 11;
    case "Q":
      return 12;
    case "K":
      return 13;
    default:
      return parseInt(cardNum);
  }
};

// export default Card;
// module.exports = Card;
