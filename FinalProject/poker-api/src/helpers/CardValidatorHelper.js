const Card = require(`../models/Card.js`);
const ErrorResponse = require(`../models/ErrorResponse.js`);
const ArrayHelper = require(`./ArrayHelper.js`);

const CardSuitCodes = new Set(['H', 'S', 'D', 'C']);
const arrayHelper = new ArrayHelper();

module.exports = class CardValidatorHelper {
  validateCardCodeRequestStructure(cardCodesRequest) {
    validateCardCodeRequestStructure(cardCodesRequest)
  }

  validateCardCodes(cardCodesRequest) {
    validateCardCodes(cardCodesRequest);
  }

  validateCardsArray(cardsArray) {
    validateCardsArray(cardsArray);
  }
}

// PRIVATE/UNEXPOSED FUNCTIONS
const validateCardCodeRequestStructure = cardCodesRequest=> {
  if (!cardCodesRequest
      || !cardCodesRequest.body
      || !cardCodesRequest.body.hand) {
        throw "Invalid Request"
  }
}

const validateCardCode = cardCode => {
  try {
    if (!cardCode) {
      throw "Empty Card Code";
    }
  
    if (cardCode.length<2) {
      throw "Card Code is incomplete";
    }
  
    card = new Card(cardCode);

    if (!CardSuitCodes.has(card.Suit)) {
      throw `${cardCode} uses ${card.Suit} that is an invalid card suit. Please use valida card suites ("H", "S", "D", and/or "C")`;
    }
  }
  catch(error) {
    throw new ErrorResponse([cardCode], error);
  }
}

const validateCardCodes = cardCodes => {
  const distinctCardCodes = new Set(cardCodes);
  if (cardCodes.length !== distinctCardCodes.size)
  {
    const differeces = arrayHelper.getArrayDifferences(cardCodes, [...cardCodes]);
    throw new ErrorResponse(
      invalidCards = differeces,
      error = `One or more cards has a duplicate`);
    }

  cardCodes.map(cardCode => validateCardCode(cardCode));
}

const validateCardsArray = cardsArray => {
  const cardCodes = cardsArray.map(card => card.CardDisplay);
  validateCardCodes(cardCodes);
}