const Card = require(`../models/Card.js`);
const CardValidatorHelper = require(`./CardValidatorHelper.js`);
// import Card from `../models/Card.js`

const validator = new CardValidatorHelper();

module.exports = class CardHelper {
  evaluateHandCards(cardCodesRequest) {
    validator.validateCardCodeRequestStructure(cardCodesRequest);
    return evaluateCards(cardCodesRequest.body.hand);
  }
}


// PRIVATE/UNEXPOSED FUNCTIONS
const upperCaseCardCodes = cardCodes => cardCodes.map(card => card.toUpperCase());

const convertCardCodesToCardObjects = cardCodes => {
  let cardArray = [];
  cardCodes.map(cardCode => cardArray.push(new Card(cardCode)));

  const sortedCards = sortCards(cardArray);
  return sortedCards;
};

const sortCards = cards => cards.sort((a, b) => a.Num - b.Num)

const getSimilarityCount = cards => {
  const cardNums = cards.map(card => card.Num);
  const cardNumSet = new Set(cardNums);

  let numCounts = [...cardNumSet].map(cardNum => ({
    num: cardNum,
    instance: cardNums.filter(num => num == cardNum).length
  }));
  return {
    quadruples: numCounts.filter(num => num.instance == 4).length,
    triples: numCounts.filter(num => num.instance == 3).length,
    doubles: numCounts.filter(num => num.instance == 2).length
  };
};

const checkComboFlush = cards => {
  const cardSuits = new Set();
  cards.map(card => cardSuits.add(card.Suit));
  return cardSuits.size == 1;
};

const checkComboStraight = cards => {
  if (!!checkComboStraightTrial(cards)) {
    return true;
  }

  let cardsWithHighAce = []
  for (card of cards) {
    card.Num = card.AlternateNum ? card.AlternateNum : card.Num;
    cardsWithHighAce.push(card);
  }

  const sortedCardsByHighAce = sortCards(cardsWithHighAce);
  return checkComboStraightTrial(sortedCardsByHighAce);
};

const checkComboStraightTrial = cards => {
  let previousCardNum;
  let skipped = false;
  cards.map(card => {
    if (!skipped) {
      if (previousCardNum && card.Num != previousCardNum + 1) {
        skipped = true;
      }
      previousCardNum = card.Num;
    }
  });
  return !skipped;
};

const checkComboRoyal = cards => {
  const cardSet = new Set(cards.map(card => card.NumDisplay));

  if (cardSet.size != 5)
    return false;

  const royalChars = new Set([`10`, `J`, `Q`, `K`, `A`]);
  let union = new Set([...royalChars, ...cardSet]);

  return union.size == 5;
};

const evaluateCards = cardCodes => {
  cardCodes = upperCaseCardCodes(cardCodes);
  validator.validateCardCodes(cardCodes);
  let cardArray = convertCardCodesToCardObjects(cardCodes);
  validator.validateCardsArray(cardArray);


  const similarityCount = getSimilarityCount(cardArray);

  if (
    !!similarityCount.quadruples ||
    !!similarityCount.triples ||
    !!similarityCount.doubles
  ) {
    const gotFourOfAKind = !!similarityCount.quadruples;
    const gotFullHouse = !!similarityCount.triples && !!similarityCount.doubles;
    const gotThreeOfAKind = !!similarityCount.triples && !similarityCount.doubles;
    const gotTwoPair = similarityCount.doubles == 2;
    const gotPair = similarityCount.doubles == 1;

    switch (true) {
      case gotFourOfAKind:
        return `Four of a Kind`;
      case gotFullHouse:
        return `Full House`;
      case gotThreeOfAKind:
        return `Three of a Kind`;
      case gotTwoPair:
        return `Two Pair`;
      case gotPair:
        return `Pair`;
    }
  } else {
    const gotFlush = checkComboFlush(cardArray);
    const gotStraight = checkComboStraight(cardArray);
    const gotStraightFlush = gotFlush && gotStraight;
    const gotRoyalFlush = gotFlush && checkComboRoyal(cardArray);

    switch (true) {
      case gotRoyalFlush:
        return `Royal Flush`;
      case gotStraightFlush:
        return `Straight Flush`;
      case gotFlush:
        return `Flush`;
      case gotStraight:
        return `Straight`;
    }
  }

  return `High card`;
};
