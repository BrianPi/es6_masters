let CardHelper = require("../helpers/CardHelper.js");

cardHelper = new CardHelper();

console.clear();
// console.clear();
console.log(cardHelper.evaluateHandCards(["C10", "CJ", "CQ", "CK", "CA"]));
console.log(cardHelper.evaluateHandCards(["C3", "C4", "C5", "C6", "C7"]));
console.log(cardHelper.evaluateHandCards(["C3", "C4", "C5", "C7", "C8"]));
console.log(cardHelper.evaluateHandCards(["C3", "S4", "D5", "H6", "C7"]));

console.log(cardHelper.evaluateHandCards(["C7", "S7", "D7", "H7", "S8"]));
console.log(cardHelper.evaluateHandCards(["C7", "S7", "D7", "H8", "S8"]));
console.log(cardHelper.evaluateHandCards(["C7", "S7", "D7", "H9", "S8"]));
console.log(cardHelper.evaluateHandCards(["C7", "S7", "D7", "H7", "S8"]));
console.log(cardHelper.evaluateHandCards(["C7", "S7", "D9", "H8", "S8"]));
console.log(cardHelper.evaluateHandCards(["C7", "S7", "D9", "H5", "SJ"]));