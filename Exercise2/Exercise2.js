const rankLimitter = 3; // read this late, ranking can handle thousands LOL

const prettifyRank = rank => {
  // Handles things like 11th, 112th, etc
  if (rank > 10) {
    const last2Digits = rank.toString().slice(-2);
    const specialRanks = ["11", "12"];
    if (specialRanks.includes(last2Digits)) {
      return `${rank}th`;
    }
  }

  // Handles things like 21st, 132nd, 3rd, etc
  const lastDigit = rank.toString().slice(-1);

  switch (lastDigit) {
    case "1":
      return `${rank}st`;
    case "2":
      return `${rank}nd`;
    case "3":
      return `${rank}rd`;
    default:
      return `${rank}th`;
  }
};

const sortScores = scores => {
  return scores.sort((a, b) => {
    return a.points < b.points ? 1 : -1;
  });
};

const rankAndPrintScores = scores => {
  const sortedScores = sortScores(scores);
  let result = "  ";

  let rank = 1;
  let tieCounter = 0;
  let rankLimitCounter = 0;
  sortedScores.map(({ name, points }, index) => {
    // Simply because i missed the part that we only need the top 3, supported infinite XD
    if (rankLimitCounter <= rankLimitter ) {
      const nextIndex = index + 1;
      if (
        sortedScores.length > nextIndex &&
        points == sortedScores[nextIndex].points
      ) {
        tieCounter++;
      } else {
        const rankPosition = prettifyRank(rank);
        if (tieCounter == 0) {
          result += `(${rankPosition}) ${name} scored ${points} out of 10`;
        } else {
          result += `(${rankPosition}) ${tieCounter +
            1} scored ${points} out of 10`;
        }
        tieCounter = 0;
        rank++;
        result += `
  `;
      }
      rankLimitCounter++;
    }
  });
  return result;
};

// ====================================================================================================
// INPUT
// ====================================================================================================
const scores = [
  { name: "Mario", points: 3 },
  { name: "Peach", points: 5 },
  { name: "Luigi", points: 3 },
  { name: "Toad", points: 2 },
  { name: "Yoshi", points: 7 }
];

function outputTag(strings, title, output) {
  var [str0, str1, str2] = [...strings];
  return `${str0}${title}${str1}${output}${str2}`;
}

// ====================================================================================================
// OUTPUT
// ====================================================================================================

const outputTitle = "EXERCISE #2: Score ranking";
const outputForPrint = outputTag`
======================================================================
 ${outputTitle}
======================================================================
 OUTPUT:
----------------------------------------------------------------------

${rankAndPrintScores(scores)}
======================================================================

`;

// console.clear();
console.log(outputForPrint);
