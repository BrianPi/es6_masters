// Store
const [rock, paper, scissors] = ["rock", "paper", "scissors"];
const handForms = [rock, paper, scissors];
const roundResults = new Set();

// Tags
const handThrownTag = (strings, playerNumber, hand) => `Player ${playerNumber} ${hand}`;

// Helpers
const printDivider = () => console.log('----------------------------------------------------------------------------------------------------');

// Backend
const getRandomIndex = length => Math.floor(Math.random() * length);
const getRandomHandThrowSpeed = () => Math.floor(Math.random() * 1000);
const getRandomHandGesture = () => handForms[getRandomIndex(handForms.length)];

// Services
function randomHandGestureService() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(getRandomHandGesture());
    }, getRandomHandThrowSpeed());
  });
}

const getPlayerHand = async playerNumber => {
  var hand = await randomHandGestureService();
  return { playerNumber, hand };
};

const checkRoundWinner = (player1, player2) => {
    const {playerNumber: player1No, hand: player1hand} = player1;
    const {playerNumber: player2No, hand: player2hand} = player2;
    switch (player1hand) {
        case rock:
          switch (player2hand) {
            case rock:      return null;
            case paper:     return player2;
            case scissors:  return player1;
          }
          break;
        case paper:
          switch (player2hand) {
            case rock:      return player1;
            case paper:     return null;
            case scissors:  return player2;
          }
          break;
        case scissors:
          switch (player2hand) {
            case rock:      return player2;
            case paper:     return player1;
            case scissors:  return null;
          }
          break;
      }
}

const startRound = async roundNumber => {
    const player1HandThrowPromise = getPlayerHand(1);
    const player2HandThrowPromise = getPlayerHand(2);
    await Promise.all([player1HandThrowPromise, player2HandThrowPromise]).then(
      resolve => {
        const [player1, player2] = resolve;
        const roundWinner = checkRoundWinner(player1, player2);
        roundResults.add(roundWinner ?
            {
                round: roundNumber,
                winner: roundWinner.playerNumber,
                result: `Winner Player ${roundWinner.playerNumber}`
            } : {
                round: roundNumber,
                results: 'TIE'
            });
        const roundResultText = !!roundWinner
            ? `Winner Player ${roundWinner.playerNumber}`
            : `It's a tie`;

        printDivider()
        console.log(`ROUND ${roundNumber}:`)
        console.log(handThrownTag`${player1.playerNumber}${player1.hand}`)
        console.log(handThrownTag`${player2.playerNumber}${player2.hand}`);
        console.log(roundResultText)
      }
    );
}

const evaluateScores = () => {
    let winBalance = 0;
    const roundResultsArray = [...roundResults];
    roundResultsArray.map(roundResult => {
        if (roundResult.winner) {
            winBalance += roundResult.winner == 1 ? 1 : -1;
        }
    })
    let gameResultText = '';
    if (winBalance == 0) {
        gameResultText = 'TIE'
    } else {
        const winner = winBalance > 0 ? 1 : 2;
        gameResultText = `The Winner of ${roundResults.size} Rounds is Player ${winner}`;
    }
    printDivider();
    console.log(`FINAL GAME RESULTS: ${gameResultText}`);
    printDivider();
}

const startGames = async numberOfRounds => {
    console.clear();
    roundResults.clear();
    printDivider();
    console.log('ROCK x PAPER x SCISSORS BATTLE!!!')
    for (i = 1; i <= numberOfRounds; i++) {
        await startRound(i);
    }
    evaluateScores();
}

startGames(5);