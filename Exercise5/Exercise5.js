class Pokemon {
  constructor(id, name, type) {
    this.ID = id;
    this.Name = name;
    this.Type = type;
  }

//   Attack = attackname => `${this.Name} used ${attackname} and its super effective!`

    Attack(attackname) {
        return `${this.Name} used ${attackname} and its super effective!`
    }
}

const pokemons = [
	new Pokemon(1, 'Bulbasaur', 'Grass'),
	new Pokemon(2, 'Ivaysaur','Grass'),
	new Pokemon(4, 'Charmander', 'Fire'),
	new Pokemon(7, 'Squirtle', 'Water')
];

const findWhere = (objectArray, searchCriteria = {type: 'Grass'}) => {
    const {id, name, type} = searchCriteria
    result = objectArray.filter((item) => {
        // return true;
        return (
            id && item.ID.toLowerCase() == id.toLowerCase()
            || name && item.Name.toLowerCase() == name.toLowerCase()
            || type && item.Type.toLowerCase() == type.toLowerCase()
        );
    });
    
    return result;
}

console.clear();
console.log(`classArray:`);
console.log(pokemons);


const searchResults = findWhere(pokemons, {type: 'Grass'});
console.log(`
searchResults:`);
console.log(searchResults);

console.log(`
Attack Sample:`);
searchResults.map(pokemon => {
    console.log(pokemon.Attack('Tackle'));
})