// ====================================================================================================
// TAG (main exercise logic)
// ====================================================================================================
const stringCounterTag = strings => {
  const text = strings[0].trim();

  if (text.length < 50) {
    return `
    [ERROR] Invalid Input!
            You have inputed only ${text.length} characters.
            Please enter a string with atleast 50 characters
            `;
  }

  const { wordCount, consonantCount, vowelCout } = getTextCountStatistics(text);

  const result = `
    ${countTag`Number of ${`word`}(s) found: ${wordCount}`}
    ${countTag`Number of ${`vowel`}(s) found: ${consonantCount}`}
    ${countTag`Number of ${`consonant`}(s) found: ${vowelCout}`}
    `;

  return result;
};

const countTag = (strings, countTarget, count) => {
    var [str0, str1] = [...strings];
  return `${str0}${countTarget}${str1}${count}`;
} 

// ====================================================================================================
// TAG (main exercise logic)
// ====================================================================================================

// Just thought of using map instead but didnt have time
const getTextCountStatistics = text => {
  // Just explicitly trim
  const trimmedText = text.trim();
  const vowels = "aeiou";
  const consonants = "bcdfghjklmnpqrstvwxyz";
  const wordSplitters = " "; // Experimental: avoided using .split() <see commented function below>, didnt have time to test spaces, tabs, etc.

  return {
    wordCount: countCharacterInstances(trimmedText, wordSplitters) + 1,
    vowelCout: countCharacterInstances(trimmedText, ...vowels),
    consonantCount: countCharacterInstances(trimmedText, ...consonants)
  };
};

const countCharacterInstances = (text, ...characters) => {
  let counter = 0;
  for (let char of text.toLowerCase()) {
    if (characters.includes(char)) {
      counter++;
    }
  }

  return counter;
};


// Ignored to practice ES6
// const getWordCount = text => {
//   return text.split(" ").length;
// };

// ====================================================================================================
// INPUT
// ====================================================================================================
const fail = stringCounterTag`The quick brown fox jumps over the lazy dog.`;
const pass = stringCounterTag`The quick brown fox jumps over the lazy dog. The quick brown fox jumps over the lazy dog`;

// ====================================================================================================
// OUTPUT
// ====================================================================================================
// NOTE:    Already made and use 2 tag templates above.
//          Didn't use tags below to avoid confusion.
//          Use tags for printing the output for exercise 2 though.
console.clear();
console.log(`
======================================================================
 EXERCISE #1: String Count Statistics ES6 Tag
======================================================================
 OUTPUT USING CORRECT INPUT:
----------------------------------------------------------------------
${pass}
======================================================================
 OUTPUT USING INVALID INPUT:
----------------------------------------------------------------------
${fail}
======================================================================

`);
