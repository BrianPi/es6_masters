const pokemons = [
  { id: 1, name: "Bulbasaur", type: "Grass" },
  { id: 2, name: "Ivaysaur", type: "Grass" },
  { id: 4, name: "Charmander", type: "Fire" },
  { id: 7, name: "Squirtle", type: "Water" }
];

const findWhere = (objectArray, searchCriteria = {type: 'Grass'}) => {
  const { id, name, type } = searchCriteria;
  result = objectArray.filter(item => {
    return (
      (id && item.id.toLowerCase() == id.toLowerCase()) ||
      (name && item.name.toLowerCase() == name.toLowerCase()) ||
      (type && item.type.toLowerCase() == type.toLowerCase())
    );
  });

  return result;
};

// const searchResults = findWhere(pokemons, { type: "Grass" });
const searchResults = findWhere(pokemons);

console.clear();
console.log(result);
