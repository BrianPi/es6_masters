// PLUCK FUNCTION
const pluck = (arr, key) => arr.map(arrItem => arrItem[key]);

// TEST DATA
const paints = [{color: 'red'}, {color: 'blue'},{color: 'green'}];

// TEST FUNCTION
const result = pluck(paints, 'color');

// OUTPUT
console.clear();
console.log("PLUCK RESULT", result);